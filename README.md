En esta edición de Connect 2019 vamos a hacer un torneo de FIFA para cambiar un poco la dinámica de charlas, distender y al mismo tiempo, inventar algo nuevo para identificar futuros talentos.

# Copa Connect: FIFA 18 World Cup

Tomando como referencia el último mundial, se jugará esa edición del famoso juego de EA Sports, iniciando en el primer round de eliminación del torneo según lo que ocurrió en la realidad, con estos 16 equipos.

![image](https://www.notion.so/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F085fc2d9-c4b0-4288-adf4-62ab0d96efe1%2Fllaves-nuevo.png?table=block&id=9fce88d3-159c-40d7-bfa7-5f73eabfbfbd&width=4800&cache=v2)

Enviaremos un link de solicitud de participación a nuestros partners y clientes, estableciendo un límite de 2 jugadores por empresa, que si lo manifiestan pueden compartir equipo.

De todos los registros solicitados, serán invitados a participar de forma aleatoria (pero sin exceder el límite de 2 jugadores por empresa) los jugadores necesarios para controlar a los 16 equipos de la fase inicial.

Una vez comunicados los equipos a controlar por los jugadores, estos tendrán que determinar una alineación inicial y una formación, a mantener dentro de lo posible durante el torneo. La fecha límite para esto es el 25 de Agosto, 2019.

## Copa Interlink: Predicciones con Inteligencia Artificial, una red neuronal usando Node.JS, React + Python.

Por otra parte, existirá una competencia paralela que intentará predecir el resultado de cada partido según los datos iniciales, empleando una red neuronal basada en TensorFlow.

Los participantes tendrán que solicitar registración igual que los de la Copa Connect, indicando habilidades, enviando su perfil de GitHub, GitLab, o bien adjuntando un CV. 

Se seleccionarán 12 personas, que tendrán la posibilidad de preparar los elementos necesarios 10 días antes del torneo. Luego durante Interlink Connect deberán socializar entre sí y conformar 4 equipos con 1 capitán cada uno. 

El capitán es designado por cada equipo, y generalmente debiera ser quien tenga mejor capacidad de organizar los esfuerzos o bien mayores conocimientos técnicos.

## Requisitos

Conocimientos de Python y [TensorFlow](https://www.tensorflow.org) para preparación de modelo y análisis de datos. 

Node.JS, React y librerías de gráficos para ilustrar resultados.

## Dataset

Usaremos el dataset completo de los jugadores de FIFA 18:

[Kaggle: Your Home for Data Science](https://www.kaggle.com/kevinmh/fifa-18-more-complete-player-dataset/downloads/fifa-18-more-complete-player-dataset.zip/5)

Gracias a esto, tendremos un CSV con todos los atributos de los jugadores disponibles, y podremos adaptarlo a las formaciones enviadas por los jugadores de la Copa Interlink. 

## Repositorio Principal

Utilizando y adaptando Deep Neural Network (DNN) de Andrew Carter, podremos hacer un modelo de datos para luego efectuar predicciones sobre ese CSV y esas formaciones. 

[AndrewCarterUK/football-predictor](https://github.com/AndrewCarterUK/football-predictor)

Si bien el uso original del repositorio es distinto, el desafío consiste en utilizar [dataset.py](http://dataset.py) para hacer un modelo de datos que nos permita emplear data de prueba, entrenar esa red neuronal y hacer las predicciones más sofisticadas posibles sobre este torneo de FIFA 18, la Copa Connect.

[A Beginners Guide to Beating the Bookmakers with TensorFlow](https://andrew.carterlunn.co.uk/programming/2018/02/20/beating-the-bookmakers-with-tensorflow.html)

## Datos de Prueba

Para realizar pruebas de predicciones y compararlas con un dataset distinto, recomendamos utilizar este dataset de los resultados reales de la Copa del Mundo 2018: 

[FIFA [World Cup 18]](https://www.kaggle.com/shreyanshu/fifa-world-cup-18)

Como una opción más avanzada, se aceptan tambien simulaciones sobre el dataset original de los jugadores del juego de EA Sports. En ese caso se deberán compilar formaciones con los datos provistos por la Copa Connect, y simular muchas veces los posibles resultados. 

## Coeficientes de Rendimiento Según la Información Disponible

Como elemento modificador de posibles desenlaces, contaremos con el input de los jugadores del torneo de FIFA (Copa Connect), que nos proveerán con una formación predilecta, y una secundaria. 

Con ello podemos aplicar un coeficiente de peso para determinar las probabilidades de un desenlace u otro, siguiendo los parámetros de James Le, para tener ratings promedio por cada selección según la formación.

[FIFA World Cup 2018: A Data-Driven Approach to Ideal Team Line-Ups](https://towardsdatascience.com/fifa-world-cup-2018-a-data-driven-approach-to-ideal-team-line-ups-93505cfe36f8)

De este modo podremos influenciar nuestras predicciones, si tenemos por ejemplo una diferencia relevante entre Argentina con un 4-3-3 (promedio general de 84.27) contra Bélgica usando un 4-3-1-2, que le brinda un promedio general de 83.09

![image](https://www.notion.so/image/https%3A%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F2ba5e0ac-56b2-495c-a97a-8e1ae35d881d%2FScreenshot_2019-07-10_at_12.20.07.png?table=block&id=937f1749-3ab4-4836-b88f-4b7835195c54&width=2370&cache=v2)

Esto estará sin duda afectado por el estilo de juego y la habilidad de cada participante. Para ello sugerimos establecer un grado de aptitud general, basado en tres niveles: Principante, Intermedio y Avanzado.

## Premios

El ganador de la Copa Connect recibirá una Playstation 4 como premio, y el ganador de la Copa Interlink, un iPad al capitán del equipo ganador. 

Como mención especial, los equipos que consigan predecir correctamente al jugador de FIFA (el nombre dentro del dataset) que sea goleador del torneo, serán invitados a conocer las oficinas de Interlink en Rosario y podrán solicitar una entrevista para saber más sobre la empresa, con la posibilidad de explorar oportunidades de trabajo con la misma.